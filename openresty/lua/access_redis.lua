local redis = require("resty.redis");
local common_utils = require("common_utils");

local red = redis:new()
red:set_timeout(1000)

local ok, err = red:connect(common_utils.get_ip("redis"), 6379)
if not ok then
    ngx.say("failed to connect: ", err)
    return
end

local res, err = red:auth("docker")
if not res then
    ngx.say("failed to autoenticate: ", err)
    return
end

red:init_pipeline()
red:set("a", "1")
red:set("b", "2")
red:hset("a s s")
red:get("a")
red:get("b")
red:get("c")

local results, err = red:commit_pipeline()
if not results then
    ngx.say("failed to commit the pipelined requests: , err")
    return
end

for i, res in ipairs(results) do
    if type(res) == "table" then
        if res[1] == false then
            ngx.say("failed to run command ", i, ": ", res[2])
        end
    else
        ngx.say("type: ", type(res), "---res: ", res)
    end
end

local ok, err = red:set_keepalive(10000,100)
if not ok then
    ngx.say("failed to set keepalive: ", err);
    return
end
