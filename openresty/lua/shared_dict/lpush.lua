local shared_test = ngx.shared.shared_test;

local newval, err = shared_test:incr("incr_init_n", 1, 0)
local length, err = shared_test:lpush("push_abc", newval)

ngx.say("length:", length, " lpush_value:", newval)
