local shared_test = ngx.shared.shared_test;

local val, err = shared_test:lpop("push_abc")
local len, err = shared_test:llen("push_abc")

ngx.say("length:", len, " lpush_value:", val)
