local pg = require("resty.postgres");
local config = require("config");

local db, err = pg:new();
if not db then
    ngx.say("failed to instantiate postgresql: ", err);
end
db:set_timeout(1000);

local ok, err = db:connect(config.postgres_config)
if not ok then
    ngx.say(err);
    return
end

local res, err = db:query("SELECT id, host FROM nginx_resource LIMIT 2")

local cjson = require("cjson");
ngx.say("result: ", type(res));
ngx.say("result: ", cjson.encode(res));

-- 配置連接池
-- 設置連接池的大小為50，最大空閒時間是10s
local ok, err = db:set_keepalive(10000,50)
if not ok then
    ngx.say("failed to set keepalive: ", err);
    return
end
