local ngx = require("ngx");

local white_list_ip = ngx.shared.white_list_ip
local ip = ngx.var.remote_addr
local value, flage = white_list_ip:get(ip)
if value then
    ngx.exit(ngx.OK)
else
    ngx.exit(ngx.HTTP_FORBIDDEN)
end
