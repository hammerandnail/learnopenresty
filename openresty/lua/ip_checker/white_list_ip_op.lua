local ngx = require("ngx");

local white_list_ip = ngx.shared.white_list_ip
local op = ngx.var.arg_op
local ip = ngx.var.arg_ip

if op == "add" then
    white_list_ip:set(ip, "1")
elseif op == "del" then
    white_list_ip:delete(ip)
end

local ds = white_list_ip:get_keys()
if type(ds) == "table" then
    for _, k in ipairs(ds) do
        ngx.say("white ip: ", k)
    end
end
