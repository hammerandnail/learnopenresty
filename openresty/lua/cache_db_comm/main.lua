local host_deny = require("host_deny")
local ngx = require("ngx")
local host = ngx.var.host

local white_host = host_deny.fromcache(host)
if not white_host then
    ngx.exit(ngx.HTTP_FORBIDDEN)
else
    ngx.exit(ngx.OK)
end
