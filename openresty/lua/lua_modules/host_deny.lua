local lrucache = require("resty.lrucache")
local db_op = require("db_op")

local _M = {}

local cache, err = lrucache.new(1000) -- 可緩存1000個key
if not cache then
    return errer("failed to connect the cache: " .. (err or "unknown"))
end

local function mem_set(host)
    local sql = string.format([[SELECT PG_SLEEP(3), host FROM nginx_resource WHERE host = '%s' LIMIT 1]], host)
    local res = db_op.getPgSQL(sql)
    if type(res) == "table" then
        for i, data in ipairs(res) do
            cache:set(data["host"], "find", 5)
        end
    end
    return
end

local function mem_get(host)
    local res_host, stale_data = cache:get(host)
    if res_host then
        return res_host
    elseif stale_data then
        mem_set(host)
        res_host = cache:get(host)
        return res_host
    else
        mem_set(host)
        res_host = cache:get(host)
        return res_host
    end
end

function fromcache(host)
    local res_host = mem_get(host)
    return res_host
end

_M.fromcache = fromcache

return _M
