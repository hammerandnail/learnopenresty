local lrucache = require("resty.lrucache")
local db_op = require("db_op")

local _M = {}

local db_locks = ngx.shared.db_locks
local cache = ngx.shared.db_cache

local function get_PgSQL(host)
    local sql = string.format([[SELECT PG_SLEEP(3), host FROM nginx_resource WHERE host = '%s' LIMIT 1]], host)
    local res = db_op.getPgSQL(sql)
    if res[1] then
        local value = res[1]["host"] or nil
        return value
    end
    return nil
end

local function lock_db(key)
    local resty_lock = require("resty.lock")
    local lock, err = resty_lock:new("db_locks")
    if not lock then
        ngx.log(ngx.ERR, err)
        return nil, "failed to create lock: " .. err
    end

    local elapsed, err = lock:lock(key)
    if not elapsed then
        ngx.log(ngx.ERR, err)
        return nil, "failed to acquire the lock: " .. err
    end
    ngx.log(ngx.ERR, elapsed)

    local val, err = cache:get(key)
    if val then
        local ok, err = lock:unluck()
        if not ok then
            ngx.log(ngx.ERR, err)
            return nil, "failed to unlock: " .. err
        end
        return val
    end
    local val = get_PgSQL(key)
    if not val then
        local ok, err = lock:unlock()
        if not ok then
            ngx.log(ngx.ERR, err)
            return nil, "failed to unluck: " .. err
        end
        local ok, err = cache:set(key, "null", 1)
        return "null"
    end
    local ok, err = cache:set(key, val, 3)
    if not ok then
        if not ok then
            return nil, "failed to unlock: " .. err
        end
        return nil, "failed to update shm cache: " .. err
    end
    local ok, err = lock:unlock()
    if not ok then
        return nil, "failed to unlock: " .. err
    end
    return val
end

local function mem_get(host)
    local res_host = cache:get(host)
    if res_host then
        return res_host
    else
        local res_host = lock_db(host)
        return res_host
    end
end

local function fromcache(host)
    local res_host = mem_get(host)
    return res_host
end

_M.fromcache = fromcache

return _M
