local lrucache = require("resty.lrucache");

local _M = {}

local cache, err = lrucache.new(1000)
if not cache then
    return error("failed to create the cache: " .. (err or "unknown"))
end

local function mem_set(key)
    cache:set("a", 19, 2)  -- (key, value, 時效(2s))
    cache:set("b", {"1", "2", "3"}, 0.001)
    return
end

local function mem_get(key)
    local a, stale_data = cache:get(key)
    return a, stale_data
end

function fromcache()
    local a, stale_data = mem_get("a")
    if a then
        ngx.say("a: ", a)
    elseif stale_data then
        ngx.say("a is expired: ", stale_data)
        mem_set()
        local a_agent = mem_get("a")
        ngx.say("a: ", a_agent)
    else
        ngx.say("no found a")
        mem_set()
        local a_agent = mem_get("a")
        ngx.say("a: ", a_agent)
    end
end

_M.fromcache = fromcache;

return _M
