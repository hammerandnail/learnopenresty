local common_utils = require("common_utils");

local _M = {}

local postgres_config = {
    host = common_utils.get_ip("database"),
    port = 5432,
    database = "resty.postgres",
    user = "postgres",
    password = "docker",
    compact = false
}

_M.postgres_config = postgres_config;

return _M
