local config = require("config")

local _DB = {}

function getPgSQL(sql)
    local PgSQL = require("resty.postgres")
    local db, err = PgSQL:new()
    if not db then
        ngx.say("failed to instantiate PostgreSQL: ", err)
        return
    end
    db:set_timeout(5000)

    local ok, err = db:connect(config.postgres_config)
    if not ok then
        ngx.say("failed to connect: ", err)
        return
    end

    local sql = sql
    local res, err = db:query(sql)
    if not res then
        ngx.say("bad result: ", err)
        return
    end

    ngx.log(ngx.ERR, db:get_reused_times(), err)
    local ok, err = db:set_keepalive(10000, 10)
    if not ok then
        ngx.say("failed to set keepalive: ", err)
        return
    end
    return res
end

_DB.getPgSQL = getPgSQL

return _DB
