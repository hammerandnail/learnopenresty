local fn = {}

function get_ip(target)
    f = assert(io.popen("bash /usr/local/openresty/nginx/lua/attachment/getIp.sh " .. target))
    for line in f:lines() do
        database_ip = line
        break
    end
    return database_ip
end

fn.get_ip = get_ip;

return fn